﻿using Category.MicroService.Handlers;
using Category.MicroService.Models;
using Category.MicroService.Queries;
using Category.MicroService.Services;
using Moq;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Category.MicroService.Test.Queries
{
    public class GetCategoriesRequestHandlerTest
    {
        private readonly Mock<ICategoryService> _mockRepo;

        public GetCategoriesRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCategoryService();
        }

        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetCategoriesHandler(_mockRepo.Object);

            var result = await handler.Handle(new GetCategoriesQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomCategory>>();

            result.Count().ShouldBe(2);
        }
    }
}
