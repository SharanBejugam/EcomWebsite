﻿using Login.MicroService.Handlers;
using Login.MicroService.Models;
using Login.MicroService.Queries;
using Login.MicroService.Services;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Login.MicroService.Test.Queries
{
    public class GetLoginRequestHandlerTest
    {
        private readonly Mock<ILoginService> _mockRepo;

        public GetLoginRequestHandlerTest()
        {
            _mockRepo = Mock.MockRepository.GetLoginService();
        }

        [Fact]
        public async Task GetLoginTest()
        {
            var handler = new GetByIdHandler(_mockRepo.Object);

            var result = await handler.Handle(new GetByIdQuery(), CancellationToken.None);

            await result.ShouldBeOfType<Task<EcomLogin>>();
        }
    }
}
