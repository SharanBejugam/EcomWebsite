﻿using Products.MicroService.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products.MicroService.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly EComContext _db;

        public ProductRepository(EComContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<EcomProducts>> GetAll()
        {
            return await Task.FromResult(_db.EcomProducts.ToList());
        }
    }
}
