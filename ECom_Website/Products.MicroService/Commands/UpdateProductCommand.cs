﻿using MediatR;
using Products.MicroService.Models;
using System.Collections.Generic;

namespace Products.MicroService.Commands
{
    public class UpdateProductCommand : IRequest<IEnumerable<EcomProducts>>
    {
        public EcomProducts Product { get; set; }
    }
}
