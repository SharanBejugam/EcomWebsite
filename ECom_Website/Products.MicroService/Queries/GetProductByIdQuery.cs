﻿using MediatR;
using Products.MicroService.Models;

namespace Products.MicroService.Queries
{
    public class GetProductByIdQuery : IRequest<EcomProducts>
    {
        public int ProductId { get; set; }
    }
}
