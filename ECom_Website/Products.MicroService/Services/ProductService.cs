﻿using Products.MicroService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Products.MicroService.Services
{
    public class ProductService : IProductService
    {
        private readonly EComContext _db;

        public ProductService(EComContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<EcomProducts>> GetAllProducts()
        {
            return await Task.FromResult(_db.EcomProducts.ToList());
        }

        public async Task<EcomProducts> GetById(int id)
        {
            return await Task.FromResult(_db.EcomProducts.SingleOrDefault(x => x.ProductId == id));
        }

        public async Task<IEnumerable<EcomProducts>> AddProduct(EcomProducts product)
        {
            _db.EcomProducts.Add(product);
            await _db.SaveChangesAsync();
            return await GetAllProducts();
        }

        public async Task<EcomProducts> DeleteProductById(int id)
        {
            var del = await GetById(id);
            _db.EcomProducts.Remove(del);
            await _db.SaveChangesAsync();
            return del;
        }

        public async Task<IEnumerable<EcomProducts>> UpdateProduct(EcomProducts product)
        {
            _db.EcomProducts.Update(product);
            await _db.SaveChangesAsync();
            return await GetAllProducts();
        }

        public async Task<IEnumerable<EcomProducts>> GetProductByCategoryId(int categoryId)
        {
            return await Task.FromResult(_db.EcomProducts.Where(x => x.CategoryId == categoryId));
        }
    }
}
