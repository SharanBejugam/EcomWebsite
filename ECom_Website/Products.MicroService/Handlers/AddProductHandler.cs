﻿using MediatR;
using Products.MicroService.Commands;
using Products.MicroService.Models;
using Products.MicroService.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Products.MicroService.Handlers
{
    public class AddProductHandler : IRequestHandler<AddProductCommand, IEnumerable<EcomProducts>>
    {
        private readonly IProductService _productService;

        public AddProductHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            return await _productService.AddProduct(request.Product);
        }
    }
}
