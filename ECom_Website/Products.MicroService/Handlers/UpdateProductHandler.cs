﻿using MediatR;
using Products.MicroService.Commands;
using Products.MicroService.Models;
using Products.MicroService.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Products.MicroService.Handlers
{
    public class UpdateProductHandler : IRequestHandler<UpdateProductCommand, IEnumerable<EcomProducts>>
    {
        private readonly IProductService _productService;

        public UpdateProductHandler(IProductService productService)
        {
            _productService = productService;
        }

        public async Task<IEnumerable<EcomProducts>> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            return await _productService.UpdateProduct(request.Product);
        }
    }
}
