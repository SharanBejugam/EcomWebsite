﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Products.MicroService.Models
{
    public partial class EcomPayment
    {
        public int PaymentId { get; set; }
        public int OrderId { get; set; }
        public string PaymentMode { get; set; }
        public string CardNumber { get; set; }
        public int? CardCvv { get; set; }
        public DateTime? CardExpiry { get; set; }
        public string CardName { get; set; }

        public virtual EcomOrders Order { get; set; }
    }
}
