﻿using Orders.MicroService.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orders.MicroService.Services
{
    public class OrderService : IOrderService
    {
        private readonly EComContext _db;

        public OrderService(EComContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<EcomOrders>> GetAllById(int custid)
        {
            return await Task.FromResult(_db.EcomOrders.Where(x => x.CustomerId == custid));
        }

        public async Task<EcomOrders> AddOrder(EcomOrders order)
        {
            _db.EcomOrders.Add(order);
            _db.SaveChanges();
            return await Task.FromResult(_db.EcomOrders.AsEnumerable().LastOrDefault());
        }
    }
}
