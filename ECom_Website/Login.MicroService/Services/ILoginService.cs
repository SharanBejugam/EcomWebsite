﻿using Login.MicroService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Login.MicroService.Services
{
    public interface ILoginService
    {
        Task<EcomLogin> AddLogin(EcomLogin login);
        Task<EcomLogin> GetByUserId(string userid);
    }
}