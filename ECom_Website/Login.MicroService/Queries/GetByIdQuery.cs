﻿using Login.MicroService.Models;
using MediatR;

namespace Login.MicroService.Queries
{
    public class GetByIdQuery : IRequest<EcomLogin>
    {
        public string UserId { get; set; }
    }
}
