﻿using Login.MicroService.Models;
using MediatR;
using System.Collections.Generic;

namespace Login.MicroService.Commands
{
    public class AddLoginCommand : IRequest<EcomLogin>
    {
        public EcomLogin Login { get; set; }
    }
}
