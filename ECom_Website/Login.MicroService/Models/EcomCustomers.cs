﻿using System;
using System.Collections.Generic;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Login.MicroService.Models
{
    public partial class EcomCustomers
    {
        public EcomCustomers()
        {
            EcomOrders = new HashSet<EcomOrders>();
        }

        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerEmailId { get; set; }
        public int LoginId { get; set; }

        public virtual EcomLogin Login { get; set; }
        public virtual ICollection<EcomOrders> EcomOrders { get; set; }
    }
}
