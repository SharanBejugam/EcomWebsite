﻿using Login.MicroService.Commands;
using Login.MicroService.Models;
using Login.MicroService.Services;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Login.MicroService.Handlers
{
    public class AddLoginHandler : IRequestHandler<AddLoginCommand, EcomLogin>
    {
        private readonly ILoginService _loginService;

        public AddLoginHandler(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public async Task<EcomLogin> Handle(AddLoginCommand request, CancellationToken cancellationToken)
        {
            return await _loginService.AddLogin(request.Login);
        }
    }
}
