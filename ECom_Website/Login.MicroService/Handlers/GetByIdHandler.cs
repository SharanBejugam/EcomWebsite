﻿using Login.MicroService.Models;
using Login.MicroService.Queries;
using Login.MicroService.Services;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Login.MicroService.Handlers
{
    public class GetByIdHandler : IRequestHandler<GetByIdQuery, EcomLogin>
    {
        private readonly ILoginService _loginService;

        public GetByIdHandler(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public async Task<EcomLogin> Handle(GetByIdQuery request, CancellationToken cancellationToken)
        {
            return await _loginService.GetByUserId(request.UserId);
        }
    }
}
