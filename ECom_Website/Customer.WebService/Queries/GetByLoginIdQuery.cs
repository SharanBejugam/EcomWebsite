﻿using Customer.WebService.Models;
using MediatR;

namespace Customer.WebService.Queries
{
    public class GetByLoginIdQuery : IRequest<EcomCustomers>
    {
        public int Id { get; set; }
    }
}
