﻿using Category.MicroService.Commands;
using Category.MicroService.Models;
using Category.MicroService.Services;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Category.MicroService.Handlers
{
    public class AddCategoryHandler : IRequestHandler<AddCategoryCommand, IEnumerable<EcomCategory>>
    {
        private readonly ICategoryService _categoryService;

        public AddCategoryHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task<IEnumerable<EcomCategory>> Handle(AddCategoryCommand request, CancellationToken cancellationToken)
        {
            return await _categoryService.AddCategory(request.Category);
        }
    }
}
