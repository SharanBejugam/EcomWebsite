﻿using Category.MicroService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Category.MicroService.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<EcomCategory>> AddCategory(EcomCategory category);
        Task<IEnumerable<EcomCategory>> GetAll();
        Task<EcomCategory> GetById(int id);
    }
}