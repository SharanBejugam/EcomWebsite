﻿using Category.MicroService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Category.MicroService.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly EComContext _db;

        public CategoryService(EComContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<EcomCategory>> GetAll()
        {
            return await Task.FromResult(_db.EcomCategory.ToList());
        }

        public async Task<EcomCategory> GetById(int id)
        {
            return await Task.FromResult(_db.EcomCategory.SingleOrDefault(x => x.CategoryId == id));
        }

        public async Task<IEnumerable<EcomCategory>> AddCategory(EcomCategory category)
        {
            _db.EcomCategory.Add(category);
            await _db.SaveChangesAsync();
            return await GetAll();
        }
    }
}
