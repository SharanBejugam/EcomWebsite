﻿using Moq;
using Orders.MicroService.Models;
using Orders.MicroService.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orders.MicroService.Test.Mock
{
    public static class MockRepository
    {
        public static Mock<IOrderService> GetLoginService()
        {
            var catlist = new List<EcomOrders>
            {
                new EcomOrders
                {
                    OrderId = 1,
                    ProductId = 101,
                    CustomerId = 201,
                    OrderQuantity = 2,
                    OrderPrice = 2000,
                    ShipmentAddress = "Bangalore"
                },
                new EcomOrders
                {
                    OrderId = 1,
                    ProductId = 101,
                    CustomerId = 201,
                    OrderQuantity = 2,
                    OrderPrice = 2000,
                    ShipmentAddress = "Bangalore"
                }
            };

            var mockrepo = new Mock<IOrderService>();

            mockrepo.Setup(r => r.GetAllById(101).Result).Returns(catlist);

            return mockrepo;
        }
    }
}
