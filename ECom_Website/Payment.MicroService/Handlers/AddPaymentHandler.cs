﻿using MediatR;
using Payment.MicroService.Commands;
using Payment.MicroService.Models;
using Payment.MicroService.Services;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Payment.MicroService.Handlers
{
    public class AddPaymentHandler : IRequestHandler<AddPaymentCommand, IEnumerable<EcomPayment>>
    {
        private readonly IPaymentService _paymentService;

        public AddPaymentHandler(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task<IEnumerable<EcomPayment>> Handle(AddPaymentCommand request, CancellationToken cancellationToken)
        {
            return await _paymentService.AddPayment(request.Payment);
        }
    }
}
