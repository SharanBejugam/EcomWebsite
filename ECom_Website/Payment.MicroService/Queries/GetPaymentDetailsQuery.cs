﻿using MediatR;
using Payment.MicroService.Models;
using System.Collections.Generic;

namespace Payment.MicroService.Queries
{
    public class GetPaymentDetailsQuery : IRequest<IEnumerable<EcomPayment>>
    {
    }
}
