﻿using MediatR;
using Payment.MicroService.Models;

namespace Payment.MicroService.Queries
{
    public class GetPaymentByIdQuery : IRequest<EcomPayment>
    {
        public int Id { get; set; }
    }
}
