﻿using MediatR;
using Payment.MicroService.Models;
using System.Collections.Generic;

namespace Payment.MicroService.Commands
{
    public class AddPaymentCommand : IRequest<IEnumerable<EcomPayment>>
    {
        public EcomPayment Payment { get; set; }
    }
}
