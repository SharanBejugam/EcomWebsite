﻿using Payment.MicroService.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Payment.MicroService.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly EComContext _db;

        public PaymentService(EComContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<EcomPayment>> GetAll()
        {
            return await Task.FromResult(_db.EcomPayment.ToList());
        }

        public async Task<IEnumerable<EcomPayment>> AddPayment(EcomPayment payment)
        {
            _db.EcomPayment.Add(payment);
            await _db.SaveChangesAsync();
            return await GetAll();
        }

        public async Task<EcomPayment> GetById(int id)
        {
            return await Task.FromResult(_db.EcomPayment.SingleOrDefault(x => x.PaymentId == id));
        }
    }
}
